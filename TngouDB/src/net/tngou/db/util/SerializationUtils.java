package net.tngou.db.util;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 对象序列化工具包
 *
 * @author winterlau
 */
public class SerializationUtils {

    private final static Logger log = LoggerFactory.getLogger(SerializationUtils.class);
    private static Serializer g_ser;

    public static void main(String[] args) throws IOException {
        
    	ResultSet obj = new ResultSet();
    	obj.setMsg("陈磊");
    	obj =(ResultSet) SerializationUtils.deserialize( SerializationUtils.serialize(obj ));
		System.err.println(obj.getMsg());
    }

    static {
       
           g_ser = new FSTSerializer(); //使用的是 FST 实例化工具
      
    }

    public static byte[] serialize(Object obj) throws IOException {
        return g_ser.serialize(obj);
    }

    public static Object deserialize(byte[] bytes) throws IOException {
        return g_ser.deserialize(bytes);
    }

}
