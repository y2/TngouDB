package net.tngou.db.cache;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;




public interface CacheEngine {

	static Map<String, Object> map= new HashMap<String, Object>();
	public  void stop() ;
	public  void add( String key, Object value);
	public  Object get( String key);
	public  void remove( String key) ;
	public  void remove() ;
	
	
	
}
