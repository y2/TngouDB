package net.tngou.db.test;

import net.tngou.db.query.Query;
import net.tngou.db.util.ResultSet;

public class Drop {
	public static void main(String[] args) {
		Query query = new Query();
		String sql = "drop table tngou";
		ResultSet r = query.sql(sql);
		System.err.println(r);
	}
}
