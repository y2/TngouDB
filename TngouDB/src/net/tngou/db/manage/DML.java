package net.tngou.db.manage;

import java.util.List;

import net.tngou.db.entity.Field;
import net.tngou.db.i18n.I18N;
import net.tngou.db.util.ResultSet;

/**
 * 数据操作语言（DML）

         修改数据库中的数据，
         包括插入(INSERT)、
         更新(UPDATE)
         和删除(DELETE)

 * @author tngou
 *
 */
public class DML extends SQL {
	
	/**
	 * 
	* @Title: insert
	* @Description:  数据操作语言  添加 删除 修改
	* @param @return    设定文件
	* @return Response    返回类型
	* @throws
	 */
	public  ResultSet 	insert() {
		
		String table=request.getParameterString("table");
		
		@SuppressWarnings("unchecked")
		List<Field> list=(List<Field>) request.getParameter("fields");
		synchronized(table){
		int status=luceneManage.add(list , table);	
		response.setStatus(status);	
		}
		response.setMsg(I18N.getValue("save"));
		
		removeCache(table);
		
		return response;
		
	}


	public ResultSet delete() {
		String table = request.getParameterString("table");
		
		Field field=(Field) request.getParameter("field");
		synchronized(table){
			int status= luceneManage.delete(field, table );
			 response.setStatus(status);	
		}
		
		response.setMsg(I18N.getValue("delete"));
		removeCache(table);
		return response;
	}

	
	public ResultSet update() {
		String table = request.getParameterString("table");
		
		
		Field term = (Field) request.getParameter("term");
		@SuppressWarnings("unchecked")
		List<Field> list=(List<Field>) request.getParameter("fields");
		Field[] updates= (Field[]) list.toArray(new Field[list.size()]);
		synchronized(table){
		 int status=luceneManage.update(table, term , updates);
		 response.setStatus(status);	
		}
		response.setMsg(I18N.getValue("update"));
		removeCache(table);
		return response;
	}

}
